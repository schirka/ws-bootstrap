#!/bin/bash

###############
## Fonctions ##

pkgs_dep(){
	# Check for required pkgs #
	for PKG in {git}; do
		if [ "$(apt list --installed 2>/dev/null | grep "^${PKG}/" | awk -F"/" '{print $1}')" != "${PKG}" ]; then
			echo "Install $PKG";
			apt-get -y install $PKG
		fi
	done
}

# folder_dep(){
# 	# Check for required folders #
# 	for FLD in {/srv/salt,/srv/salt/pillar}; do
# 		if [ ! -d $FLD ]; then
# 			echo "Create $FLD";
# 			mkdir $FLD;
# 		fi
# 	done
# }

clone(){
	git clone https://schirka@bitbucket.org/schirka/masterless-ws.git /srv/salt/
}

salt_bootstrap(){
	# Retrieve the bootstrap script from Saltstack official url
	curl -o bootstrap_salt.sh -L https://bootstrap.saltstack.com
	# Install & Configure Salt Minion to run Masterless
	sudo sh bootstrap_salt.sh -X -j '{ \
	"file_client":"local", \
	"pillar_roots":{"base":["/srv/salt/pillar"]} \
	}' stable 3000.3
}

main(){
	# Check for pkgs dependancies
	pkgs_dep
	# folder_dep
	clone
	salt_bootstrap

}

###########
## Start ##
main "@"